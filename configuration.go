package spyglasscfg
//
//import (
//	"encoding/json"
//	"log"
//	"os"
//	"path/filepath"
//	"fmt"
//)
//
//type SpyglassConfig struct {
//	IntelPort int
//	Delim     string
//}
//
//type intelConfig struct {
//	MaximumScore  int
//	IgnoreChars   []string
//	ClearWords    []string
//	BlueWords     []string
//	StatusWords   []string
//	IgnoreWords   []string
//	IntelChannels []string
//}
//
//type logConfig struct {
//	LogPath    string
//	LogChannel string
//}
//
//type mapConfig struct {
//	BaseURL string
//}
//
//type configuration struct {
//	SpyglassCfg SpyglassConfig
//	IntelCfg    intelConfig
//	LogCfg      logConfig
//	MapCfg      mapConfig
//	init        bool
//}
//
//var CONFIG configuration
//
//func (cfg configuration) readConfiguration() {
//
//	loadcfg := os.Getenv("spyglass_ignore_config")
//
//	if loadcfg == "1" {
//		log.Println("Using internal config")
//		cfg := createConfiguration()
//		cfg.init = true
//		CONFIG = cfg
//	} else {
//
//		cwd, err := os.Getwd()
//
//		if err != nil {
//			log.Fatal(err)
//			return
//		}
//
//		file, err := os.Open(filepath.Join(cwd, "spyglass_config.json"))
//
//		defer file.Close()
//
//		if err != nil {
//			log.Println(err)
//			cfg := createConfiguration()
//			cfg.init = true
//
//			cfg.saveConfiguration(filepath.Join(cwd, "spyglass_config.json"))
//
//			return
//		}
//
//		decoder := json.NewDecoder(file)
//		err = decoder.Decode(&cfg)
//
//		if err != nil {
//			log.Fatal(err)
//		}
//	}
//	CONFIG = cfg
//}
//
//func GetCfg() (config *configuration) {
//
//	if CONFIG.init != true {
//		CONFIG.readConfiguration()
//	}
//	fmt.Println(CONFIG)
//	return &CONFIG
//}
//
//func createConfiguration() (config configuration) {
//
//	spgconfig := SpyglassConfig{IntelPort: 13270, Delim: "//||//||"}
//
//	ignorechars := []string{"*", ",", "!", "."}
//	clearWords := []string{"CLEAR", "CLR"}
//	statusWords := []string{"STAT", "STATUS", "UPDATE"}
//	blueWords := []string{"BLUE", "BLUE ONLY", "BLUES ONLY", "ONLY BLUE", "STILL BLUE", "ALL BLUE", "ALL BLUES"}
//	ignoreWords := []string{"IN", "IS", "AS", "AND", "NV"}
//	chatrooms := []string{"Int.Catch", "int.testing"}
//
//	intconfig := intelConfig{MaximumScore: 4, IgnoreChars: ignorechars, ClearWords: clearWords, StatusWords: statusWords, BlueWords: blueWords, IgnoreWords: ignoreWords, IntelChannels:chatrooms}
//
//	logconfig := logConfig{LogChannel: "Chatlogs"}
//
//	mapconfig := mapConfig{BaseURL: "https://s3-ap-southeast-2.amazonaws.com/spyglass-ng/"}
//
//	config = configuration{SpyglassCfg: spgconfig, IntelCfg: intconfig, LogCfg: logconfig, MapCfg: mapconfig}
//
//	CONFIG = config
//
//	return config
//
//}
//
//func (cfg configuration) saveConfiguration(filepath string) (err error) {
//	file, err := os.Create(filepath)
//
//	if err == nil {
//		defer file.Close()
//
//		b, _ := json.MarshalIndent(cfg, "", "	")
//		fmt.Fprint(file, string(b))
//	}
//
//	return
//}
