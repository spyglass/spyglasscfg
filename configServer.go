package spyglasscfg

import (
	"database/sql"
	"log"
	_ "github.com/mattn/go-sqlite3"
	"net/http"
	"encoding/json"
	"os/user"
	"os"
	"path/filepath"
	"fmt"
	"github.com/rs/cors"
	"io/ioutil"
)

var (
	db *sql.DB
)

type SpyglassConfig struct {
	// Delimeter used in messages
	Delim string `json:"delim"`

	// The aggressiveness of word matching in intel searches
	MaximumScore int `json:"maximum_score"`

	// Characters/Words in intel parsing, comma seperated
	IgnoreChars string `json:"ignore_chars"`
	ClearWords  string `json:"clear_words"`
	BlueWords   string `json:"blue_words"`
	StatusWords string `json:"status_words"`
	IgnoreWords string `json:"ignore_words"`

	// Which intel channels to monitor, comma seperated
	IntelChannels string `json:"intel_channels"`

	// Base URL to get to maps from
	BaseURL string `json:"base_url"`

	// Jumpbridge URL or local filepath
	JumpbridgeUrl string `json:"jumpbridge_url"`

	//Path to the eve chatlogs
	LogPath string `json:"log_path"`

	//The Current Region
	Region     string `json:"region"`
	ConfigPath string `json:"config_path"`
}

func StartConfigServer() {
	config_dir := getConfigFileLocation()

	db = begin(config_dir)
	checkDB()

	defer db.Close()

	mux := http.NewServeMux()

	mux.HandleFunc("/config/get", serveConfig)
	mux.HandleFunc("/version", serveVersion)
	mux.HandleFunc("/config/overwrite", serveOverwrite)

	handler := cors.Default().Handler(mux)

	log.Println("Starting Config Server Successfully")

	http.ListenAndServe(":13273", handler)
}

func begin(dir string) (db *sql.DB) {
	db, err := sql.Open("sqlite3", filepath.Join(dir, "config.sqlite3"))
	checkErr(err)

	// Test Database Connection
	err = db.Ping()
	checkErr(err)

	//Connected to DB

	db.SetMaxOpenConns(1)

	return db

}

func checkDB() {
	query := "SELECT COUNT(*) FROM config"

	res, err := db.Query(query)

	if err != nil || !res.Next() {
		initNewDB(db)
		return
	}

	defer res.Close()

	var count int
	err = res.Scan(&count)
	checkErr(err)

	if count == 0 {
		initNewDB(db)
	}
}

func initNewDB(db *sql.DB) {
	deleteStatement := "DROP TABLE IF EXISTS config;"
	createStatement := "CREATE TABLE config (" +
		"'delim' VARCHAR(10) NOT NULL," +
		"'maximum_score' INTEGER NOT NULL," +
		"'ignore_chars' TEXT," +
		"'clear_words' TEXT," +
		"'blue_words' TEXT," +
		"'status_words' TEXT," +
		"'ignore_words' TEXT," +
		"'intel_channels' TEXT," +
		"'base_url' TEXT," +
		"'jumpbridge_url' TEXT," +
		"'log_path' TEXT," +
		"'region' TEXT" +
		");"

	delStmt, err := db.Prepare(deleteStatement)
	checkErr(err)

	crtStmt, err := db.Prepare(createStatement)
	checkErr(err)

	res, err := delStmt.Exec()
	checkErr(err)

	ra, _ := res.RowsAffected()

	log.Printf("Removed %v rows from config db", ra)

	res, err = crtStmt.Exec()
	checkErr(err)

	log.Print("Created new config table")

	insertDefaultConfig()
}

func insertDefaultConfig() {
	insertStatement := "INSERT INTO config(" +
		"delim, maximum_score, " +
		"ignore_chars, clear_words, blue_words, status_words, ignore_words, " +
		"intel_channels, base_url, jumpbridge_url, log_path, region) " +
		"values (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)"

	delim := ";;!!;;!!"
	maximum_score := 3
	ignore_chars := "*;!;.;,"
	clear_words := "CLEAR;CLR"
	blue_words := "BLUE;BLUE ONLY;BLUES ONLY;ONLY BLUE;STILL BLUE;ALL BLUE;ALL BLUES"
	status_words := "STAT;STATUS;UPDATE"
	ignore_words := "IN;AS;IS;AND;NV"
	intel_channels := "int.testing"
	base_url := "https://s3-ap-southeast-2.amazonaws.com/spyglass-ng/"
	jumpbridge_url := ""
	log_path := ""
	region := "Catch"

	stmt, err := db.Prepare(insertStatement)
	checkErr(err)

	_, err = stmt.Exec(delim, maximum_score, ignore_chars, clear_words, blue_words, status_words,
		ignore_words, intel_channels, base_url, jumpbridge_url, log_path, region)
	checkErr(err)


	log.Println("Inserted default config")
}

func serveConfig(w http.ResponseWriter, r *http.Request) {
	if r.Method != "GET" {
		http.Error(w, "Method not allowed", http.StatusBadRequest)
		return
	}

	row, err := db.Query("SELECT * FROM config LIMIT 1")
	checkIntenalServerError(err, w, "Cannot Access Config Database")

	var cfg SpyglassConfig

	row.Next()
	err = row.Scan(&cfg.Delim, &cfg.MaximumScore, &cfg.IgnoreChars, &cfg.ClearWords,
		&cfg.BlueWords, &cfg.StatusWords, &cfg.IgnoreWords, &cfg.IntelChannels,
		&cfg.BaseURL, &cfg.JumpbridgeUrl, &cfg.LogPath, &cfg.Region)
	checkIntenalServerError(err, w, "Error Parsing Config From Database Response")
	row.Close()

	w.Header().Set("Content-Type", "application/json")

	cfg.ConfigPath = getConfigFileLocation()
	json.NewEncoder(w).Encode(cfg)
}

func getConfigFileLocation() (path string) {
	usr, err := user.Current()
	checkErr(err)

	path = filepath.Join(usr.HomeDir, "spyglass")
	if _, err := os.Stat(path); os.IsNotExist(err) {
		os.Mkdir(path, 755)
	}

	return path
}

func serveVersion(w http.ResponseWriter, r *http.Request) {
	w.WriteHeader(http.StatusOK)
	fmt.Fprint(w, version)
}

func serveOverwrite(w http.ResponseWriter, r *http.Request) {
	if r.Method != http.MethodPost {
		http.Error(w, "Method not allowed", http.StatusBadRequest)
		return
	}

	var cfg SpyglassConfig

	body, err := ioutil.ReadAll(r.Body)
	checkErr(err)

	err = json.Unmarshal(body, &cfg)
	checkErr(err)

	deleteStatement := "DELETE FROM config;"

	stmt, err := db.Prepare(deleteStatement)
	checkErr(err)

	_, err = stmt.Exec()
	checkErr(err)

	insertStatement := "INSERT INTO config(" +
		"delim, maximum_score, " +
		"ignore_chars, clear_words, blue_words, status_words, ignore_words, " +
		"intel_channels, base_url, jumpbridge_url, log_path, region) " +
		"values (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)"

	stmt, err = db.Prepare(insertStatement)
	checkErr(err)

	_, err = stmt.Exec(cfg.Delim, cfg.MaximumScore, cfg.IgnoreChars, cfg.ClearWords, cfg.BlueWords, cfg.StatusWords,
		cfg.IgnoreWords, cfg.IntelChannels, cfg.BaseURL, cfg.JumpbridgeUrl, cfg.LogPath, cfg.Region)
	checkErr(err)
	log.Println("Updated config")

	w.WriteHeader(http.StatusOK)
	fmt.Fprintln(w, "Good POST")

}

func checkIntenalServerError(err error, w http.ResponseWriter, msg string) {
	if err != nil {
		http.Error(w, msg, http.StatusInternalServerError)
		panic(err)
	}
}

func checkErr(err error) {
	if err != nil {
		panic(err)
	}
}
